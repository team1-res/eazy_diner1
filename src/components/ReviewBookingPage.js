import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const ReviewBookingPage = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { selectedDate, selectedSlot, selectedGuests, restaurant } = location.state || {};
  const [mobileNumber, setMobileNumber] = useState('');
  const [isValidMobile, setIsValidMobile] = useState(true); // State to track mobile number validity

  const handleMobileNumberChange = (e) => {
    const inputValue = e.target.value;
    // Check if input value consists only of digits and is not more than 10 characters
    if (/^\d{0,10}$/.test(inputValue)) {
      setMobileNumber(inputValue);
      setIsValidMobile(true); // Reset validity when valid input is entered
    } else {
      setMobileNumber(inputValue); // Update mobile number even if invalid
      setIsValidMobile(false); // Set validity to false when invalid input is entered
    }
  };

  const handleConfirmBooking = () => {
    console.log('Confirm Booking button clicked');
  
    // Check if token is present in local storage
    const token = localStorage.getItem('token');
  
    if (token) {
      navigate(`/confirmation`, {
        state: {
          selectedDate,
          selectedSlot,
          selectedGuests,
          restaurant,
          mobileNumber,
        },
      });
    } else {
      // Redirect to signup page if token is not present
      navigate('/signup');
    }
  };
  const confirmationMessage = `You are about to confirm your booking at ${restaurant?.name}. Kindly review the details below and decide whether you want to modify your booking or confirm it.`;

  // Disable Confirm Booking button if mobile number is not exactly 10 digits
  const isMobileValid = mobileNumber.length === 10;

  return (
    <div>
      <h2>Review Booking</h2>
      <img src={restaurant?.image} alt={restaurant?.name} style={{ maxWidth: '100%' }} />
      <p>{confirmationMessage}</p>
      <p>Selected Date: {selectedDate}</p>
      <p>Selected Time: {selectedSlot?.time}</p>
      <p>Number of Guests: {selectedGuests}</p>

      {/* Input field for Mobile Number */}
      <label htmlFor="mobileNumber">Mobile Number:</label>
      <input
        type="number" // Change input type to 'number'
        id="mobileNumber"
        value={mobileNumber}
        onChange={handleMobileNumberChange}
        maxLength={10} // Restrict the length to 10 digits
        style={{ width: '200px' }}
      />
      <br />

      {/* Prompt for invalid mobile number */}
      {!isValidMobile && (
        <p style={{ color: 'red' }}>Enter a valid mobile number</p>
      )}

      <button className="orange-button" onClick={() => navigate(-1)}>
        Modify Booking
      </button>
      <br />
      <button className="orange-button" onClick={handleConfirmBooking} disabled={!isMobileValid}>
        {localStorage.getItem('token') ? 'Confirm Booking' : 'Register'}
      </button>
    </div>
  );
};

export default ReviewBookingPage;
