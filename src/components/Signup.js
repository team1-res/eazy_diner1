import React, { useState } from 'react';
import { useNavigate, Link, useLocation } from 'react-router-dom';
import axios from 'axios';

const Signup = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
  });

  const [errorMessages, setErrorMessages] = useState({
    name: '',
    email: '',
    password: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
  
    // Update the form data
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  
    // Reset any previous error messages
    setErrorMessages((prevErrors) => ({ ...prevErrors, [name]: '' }));
  
    // Check password length
    if (name === 'password' && value.length <= 8) {
      setErrorMessages((prevErrors) => ({
        ...prevErrors,
        password: 'Password must be greater than 8 characters',
      }));
    }
  };
  
  const handleSubmit = async (e) => {
    e.preventDefault();

    // Validate before submitting
    const validationErrors = validateFormData();
    if (Object.values(validationErrors).some((error) => error !== '')) {
      // Display error messages or handle invalid data
      console.error('Invalid form data:', validationErrors);
      return;
    }

    // Continue with signup
    await signupUser();
  };

  const validateFormData = () => {
    const errors = {};

    // Email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(formData.email)) {
      errors.email = 'Invalid email format';
    }

    // Null value check
    Object.keys(formData).forEach((key) => {
      if (formData[key].trim() === '') {
        errors[key] = `Value for ${key} cannot be null or empty`;
      }
    });

    // Add additional validations for other fields if needed

    // Update the error messages state
    setErrorMessages(errors);

    // Return the errors object
    return errors;
  };

  const signupUser = async () => {
    try {
      const response = await axios.post('http://localhost:8888/api/signup', formData);

      console.log('Response:', response);

      if (response && response.data) {
        console.log('Response data:', response.data);
        navigate('/login', { state: { signupSuccess: true, user: { name: formData.name } } });
      } else {
        console.error('Invalid response received from the server:', response);
      }
    } catch (error) {
      console.error('Error during signup:', error.message);
    }
  };

  // Display prompt on the login page if signup was successful
  const { state } = location;
  const signupSuccess = state && state.signupSuccess;

  // Disable signup button if password is less than 8 characters
  const isDisabled = formData.password.length < 8;

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '70vh' }}>
      <div>
        <h2>Signup Page</h2>
        {signupSuccess && <p>Signup successful! Please log in.</p>}
        <form onSubmit={handleSubmit}>
          <div>
            <label>Name:</label>
            <br />
            <input
              type="text"
              name="name"
              value={formData.name}
              onChange={handleChange}
              style={{ width: '400px' }}
            />
            {errorMessages.name && <p style={{ color: 'red' }}>{errorMessages.name}</p>}
          </div>
          <div>
            <label>Email:</label>
            <br />
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={handleChange}
              style={{ width: '400px' }}
            />
            {errorMessages.email && <p style={{ color: 'red' }}>{errorMessages.email}</p>}
          </div>
          <div>
            <label>Password:</label>
            <br />
            <input
              type="password"
              name="password"
              value={formData.password}
              onChange={handleChange}
              style={{ width: '400px' }}
            />
            {errorMessages.password && <p style={{ color: 'red' }}>{errorMessages.password}</p>}
          </div>
          <br />
          <button type="submit" disabled={isDisabled}>Sign Up</button>
        </form>

        <p>
          Already have an account? <Link to="/login">Log in</Link>
        </p>
      </div>
    </div>
  );
};

export default Signup;
