import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

const RestaurantDetails = () => {
  const { restaurantId } = useParams();
  const [restaurant, setRestaurant] = useState(null);
  const [bookingDates, setBookingDates] = useState([]);
  const [selectedDate, setSelectedDate] = useState('');
  const [bookingSlots, setBookingSlots] = useState([]);
  const [selectedSlot, setSelectedSlot] = useState(null);
  const [selectedGuests, setSelectedGuests] = useState(1); // Default to 1 guest
  const [showReviewButton, setShowReviewButton] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchRestaurantDetails = async () => {
      try {
        console.log('Fetching restaurant details...');
        const response = await fetch(`http://localhost:8088/api/restaurants/${restaurantId}`);
        const data = await response.json();
        if (response.ok) {
          setRestaurant(data.restaurant);
        } else {
          console.error('Error fetching restaurant details:', data.error);
        }
      } catch (error) {
        console.error('Error fetching restaurant details:', error);
      }
    };

    if (restaurantId) {
      fetchRestaurantDetails();
    }
  }, [restaurantId]);

  useEffect(() => {
    const fetchBookingDates = async () => {
      try {
        console.log('Fetching booking dates...');
        const response = await fetch('http://localhost:8088/api/booking/dates');
        const data = await response.json();
        if (response.ok) {
          setBookingDates(data.bookingDates);
        } else {
          console.error('Error fetching booking dates:', data.error);
        }
      } catch (error) {
        console.error('Error fetching booking dates:', error);
      }
    };

    fetchBookingDates();
  }, []);

  const fetchBookingSlots = async () => {
    try {
      console.log('Fetching booking slots...');
      const response = await fetch(`http://localhost:8088/api/booking/slots?restaurant_id=${restaurantId}&date=${selectedDate}`);
      const data = await response.json();
      if (response.ok) {
        let filteredSlots = data.bookingSlots;
  
        // Filter out past time slots if the selected date is the current date
        if (selectedDate === getCurrentDate()) {
          const currentTime = new Date().toLocaleTimeString('en-US', { hour12: false }); // Get current time in hh:mm:ss format
          filteredSlots = filteredSlots.filter(slot => slot.start_time >= currentTime);
        }
  
        setBookingSlots(filteredSlots);
      } else {
        console.error('Error fetching booking slots:', data.error);
      }
    } catch (error) {
      console.error('Error fetching booking slots:', error);
    }
  };

  const handleDateChange = (e) => {
   const selectedDate = e.target.value;
    const currentDate = new Date().toISOString().split("T")[0];

    // Check if the selected date is not in the past
    if (selectedDate >= currentDate) {
      setSelectedDate(selectedDate);
      fetchBookingSlots(selectedDate);
      setSelectedSlot(null); // Reset selected slot when the date changes
    } else {
      // If the selected date is in the past, you can show an error or simply return
      console.error('Selected date is in the past');
      // Alternatively, you can set a state to indicate an error and handle it in the UI
    }
  };

  const handleSlotSelection = (slot) => {
    setSelectedSlot(slot);
    
  };

  const handleGuestsChange = (e) => {
    setSelectedGuests(Number(e.target.value));
    setShowReviewButton(true);
  };

  const handleReviewBooking = () => {
    console.log('Selected Date:', selectedDate);
    console.log('Selected Slot:', selectedSlot);
    console.log('Selected Guests:', selectedGuests);
    navigate(`/restaurant/${restaurantId}/booking/${selectedSlot.id}/review`, {
      state: {
        selectedDate,
        selectedSlot,
        selectedGuests,
        restaurant,
      },
    });
  };
  if (!restaurantId || !restaurant || !bookingDates.length) {
     return <div>Loading...</div>;
    }

    
return (
    <div>
      <h1 className="rest-name">{restaurant.name}</h1>
      <img src={restaurant?.image} alt={restaurant?.name} style={{ maxWidth: '100%' }} />
      <p>Location: {restaurant.location}</p>
      <p>Cuisine: {restaurant.cuisine_type}</p>

      {/* Display available booking dates */}
      <h2>AVAILABLE BOOKING DATES</h2>
      <div className="search-bar">
        <select onChange={handleDateChange} value={selectedDate}>
          <option value="">Select Date</option>
          {bookingDates.map(date => (
            <option key={date} value={date}>{date}</option>
          ))}
        </select>
      </div>

{selectedDate && (
  <div>
    <h2>AVAILABLE BOOKING SLOTS</h2>
    <div className="slots-container">
      {bookingSlots.map((slot, index) => (
        <React.Fragment key={slot.id}>
          {(index > 0 && index % 8 === 0) && <br />} {/* Add a line break after every 8 slots */}
          <button
            className={`slot-button ${selectedSlot && selectedSlot.id === slot.id ? 'selected-slot' : ''}`}
            onClick={() => handleSlotSelection(slot)}
            style={{ marginRight: '10px', marginBottom: '10px' }} 
          >
            {slot.time}
          </button>
        </React.Fragment>
      ))}
    </div>
  </div>
)}




      {/* Display selected date and time */}
      {selectedDate && selectedSlot && (
        <div>
          <h3>Selected Date:</h3>
          <p>{selectedDate}</p>
          <h3>Selected Time:</h3>
          <p>{selectedSlot.time}</p>
        </div>
      )}

      {/* Add BookingForm features directly */}
      {selectedSlot && (
        <div>
          <h2>NUMBER OF VISITORS</h2>

          <div className="guest-buttons">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((guests) => (
              <button
                key={guests}
                className={`guest-button ${selectedGuests === guests ? 'selected-guests' : ''}`}
                onClick={() => handleGuestsChange({ target: { value: guests } })}
                style={{ marginRight: '8px', marginBottom: '10px', width: '50px' }}
              >
                {guests}
              </button>
            ))}
          </div>

          {/* Display "Review Booking" button if guests are selected */}
          {showReviewButton && (
            <button onClick={handleReviewBooking} className="review-booking-button">
              Review Booking
            </button>
          )}
        </div>
      )}

    </div>
  );
};
export default RestaurantDetails;
