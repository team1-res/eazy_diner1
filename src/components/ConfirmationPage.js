

import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import Confetti from 'react-confetti'; // Import Confetti component

const ConfirmationPage = () => {
  const location = useLocation();
  const { selectedDate, selectedSlot, selectedGuests, restaurant, mobileNumber } = location.state || {};
  const [reservationResponse, setReservationResponse] = useState(null);
  const [customerName, setCustomerName] = useState('');

  useEffect(() => {
    // Retrieve customer name from local storage
    const storedCustomerName = localStorage.getItem('userName');
    if (storedCustomerName) {
      setCustomerName(storedCustomerName);
    }
  }, []);

  useEffect(() => {
    const sendReservation = async () => {
      try {
        const apiUrl = 'http://localhost:8000/api/reservations';

        const reservationData = {
          slot_id: selectedSlot?.id,
          customer_name: customerName,
          contact_number: mobileNumber, // Use customer name from local storage
          booking_date: selectedDate,
          num_guests: selectedGuests,
          
        };
        

        const response = await axios.post(apiUrl, reservationData);

        console.log('Reservation response:', response.data);
        setReservationResponse(response.data);
      } catch (error) {
        console.error('Error making reservation:', error.message);
      }
    };

    sendReservation();
  }, [location.state,selectedDate, selectedSlot, selectedGuests, restaurant, mobileNumber, customerName]);

  return (
    <div style={{ display: 'flex' }}>
      {/* Add Confetti component */}
      <Confetti width={window.innerWidth} height={window.innerHeight} numberOfPieces={300} recycle={false} />
      <div style={{ flex: 1 }}>
        <h2>Booking Confirmation</h2>
        {restaurant && (
          <div>
            {/* Display customer name */}
            <p>Customer Name: {customerName}</p>
            <p>Mobile Number: {mobileNumber}</p>
            <p>Restaurant Name: {restaurant.name}</p>
            <p>Selected Date: {selectedDate}</p>
            <p>Selected Time: {selectedSlot?.time}</p>
            <p>Number of Guests: {selectedGuests}</p>
            
            
            <p style={{ fontWeight: 'bold' }}>You are requested to show up at your restaurant at least 5 minutes prior to your reporting time to avoid any inconvenience.</p>
            <p style={{ fontWeight: 'bold' }}>We have a no-show policy as follows: If you don't show up at your restaurant during the time of your booking, you will not be charged any fee. However, if you don't show up for 3 times in a row, your account will be deactivated.</p>
          </div>
        )}
      </div>
      {restaurant && (
        <div style={{ flex: 1 }}>
          {/* Adjust the size of the image */}
          <img src={restaurant.image} alt={restaurant.name} style={{ maxWidth: '400px', height: 'auto' }} />
        </div>
      )}
    </div>
  );
};

export default ConfirmationPage;


