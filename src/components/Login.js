import React, { useState, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import axios from 'axios';

const Login = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/restaurant');
    }
  }, []); // Empty dependency array ensures the effect runs only once when the component mounts

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const loginUser = async () => {
    try {
      const response = await axios.post('http://localhost:8080/api/login', formData);
  
      if (response && response.data && response.data.user) {
        const { name } = response.data.user;
        localStorage.setItem('userName', name);
        localStorage.setItem('token', response.data.token);
        console.log(response.data);
        navigate('/restaurant');
      } else {
        console.error('Invalid response received from the server:', response);
      }
    } catch (error) {
      if (error.response.status === 401) {
        setShowAlert(true);
      }
      console.error('Error during login:', error.message);
    }
  };
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log('Simulating login process:', formData);
    await loginUser();
  };

  return (
    <div>
      {showAlert && (
        <div style={{ backgroundColor: 'red', color: 'white', padding: '10px', textAlign: 'center' }}>
          Incorrect email or password
        </div>
      )}
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
        <div>
          <h2>Login Page</h2>
          <form onSubmit={handleSubmit}>
            <div>
              <label>Email:</label>
              <br />
              <input
                type="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                style={{ width: '400px' }}
              />
            </div>
            <div>
              <label>Password:</label>
              <br />
              <input
                type="password"
                name="password"
                value={formData.password}
                onChange={handleChange}
                style={{ width: '400px' }}
              />
            </div>
            <br />
            <button type="submit">Login</button>
          </form>

          <p>
            Don't have an account? <Link to="/signup">Create a new account</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Login;
