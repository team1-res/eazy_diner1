import React, { useState, useEffect } from 'react';
import RestaurantCard from './RestaurantCard';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const RestaurantListing = () => {
  const [restaurants, setRestaurants] = useState([]);
  const [cuisineType, setCuisineType] = useState('');
  const [location, setLocation] = useState('');
  const [title, setTitle] = useState('');
  const [userName, setUserName] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const storedUserName = localStorage.getItem('userName');
    if (storedUserName) {
      setUserName(storedUserName);
    }
    if (!storedUserName) {
      navigate('/login');
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, [cuisineType, location, title]);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://localhost:5000/api/restaurants', {
        params: {
          cuisineType,
          location,
          title,
        }
      });
      setRestaurants(response.data);
    } catch (error) {
      console.error('Error fetching restaurant data:', error);
    }
  };

  return (
    <div>
      <p className='welcometag'>Welcome, {userName}!</p>
      <div className="search-rest">
        <input
          type="text"
          placeholder="Search by Cuisine Type"
          value={cuisineType}
          onChange={(e) => setCuisineType(e.target.value)}
        />
        <input
          type="text"
          placeholder="Search by Location"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
        />
        <input
          type="text"
          placeholder="Search by Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="restaurant-list">
        {restaurants.map((restaurant) => (
          <div key={restaurant.id}>
            <RestaurantCard restaurant={restaurant} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default RestaurantListing;
