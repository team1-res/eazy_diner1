
const express = require('express');
const cors = require('cors');
const bcrypt = require('bcrypt');
const knex = require('knex');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js'); 

const app = express();
const port = process.env.PORT || 8080;

app.use(cors());

const db = knex(knexConfig);



app.use(bodyParser.json());

const secretKey = require('crypto').randomBytes(32).toString('hex');
console.log('Random Secret Key:', secretKey);
app.use(async (req, res, next) => {
  try {
    req.db = db;
    next();
  } catch (error) {
    console.error('Error connecting to the database:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});



app.post('/api/login', async (req, res) => {
  const { email, password } = req.body;

  try {
    const [user] = await req.db('Customers')
      .select('*')
      .where('email', email);

    if (!user) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }
    const token = jwt.sign({ userId: user.id }, secretKey, { expiresIn: '1h' });
    console.log('User logged in:', user);
    // res.json({ message: 'Login successful', token });
    res.json({ message: 'Login successful', token, user });

  } catch (error) {
    console.error('Error logging in:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.all('/api/login', (req, res) => {
  res.status(405).json({ error: 'Method Not Allowed' });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
