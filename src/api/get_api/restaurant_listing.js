const express = require('express');
const cors = require('cors');
const knex = require('knex');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js');

const app = express();
const port = process.env.PORT || 5000;

const db = knex(knexConfig);

app.use(cors());

app.get('/api/restaurants', async (req, res) => {
  try {
    const restaurants = await db('Restaurants').select('*');
    res.json(restaurants);
  } catch (error) {
    console.error('Error fetching restaurants:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/restaurants/:restaurantId/slots', async (req, res) => {
  const { restaurantId } = req.params;

  try {
    const slots = await db('Slots').where('restaurant_id', restaurantId).select('*');
    res.json(slots);
  } catch (error) {
    console.error('Error fetching slots:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(port, () => {
  console.log(`Listing Server is running on port ${port}`);
});
