const express = require('express');
const cors = require('cors');
const knex = require('knex');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile');

const app = express();
const port = 8088;
const db = knex(knexConfig);

app.use(cors());

app.get('/api/restaurants/:restaurantId', async (req, res) => {
  const restaurantId = req.params.restaurantId;

  try {
    const restaurantDetails = await db('Restaurants')
      .select('*')
      .where('id', restaurantId)
      .first();

    if (!restaurantDetails) {
      res.status(404).json({ error: 'Restaurant not found' });
    } else {
      res.json({ restaurant: restaurantDetails });
    }
  } catch (error) {
    console.error('Error fetching restaurant details:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(port, () => {
  console.log(`Details Server is running on port ${port}`);
});
