const express = require('express');
const bodyParser = require('body-parser');
const knex = require('knex');
const cors= require('cors');

// Import Knex configuration from knexfile.js
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js');

// Initialize Knex connection
const db = knex(knexConfig);

const app = express();
app.use(cors());
app.use(bodyParser.json());

// Route to create inventories for the next 5 days
app.post('/api/inventories/create', async (req, res) => {
  try {
    const fiveDaysInventories = await createInventoriesForNextFiveDays();
    res.json({ message: 'Inventories created successfully', inventories: fiveDaysInventories });
  } catch (error) {
    console.error('Error creating inventories:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Route to make a booking
app.post('/api/reservations', async (req, res) => {
  const { slot_id, customer_id, customer_name, contact_number, booking_date, num_guests } = req.body;

  try {
    // Start a transaction
    await db.transaction(async trx => {
      // Ensure inventory is available for the booking date and slot
      const availableInventory = await trx('inventories')
        .where('slot_id', slot_id)
        .andWhere('date', booking_date)
        .andWhere('quantity', '>=', num_guests) // Ensure enough quantity for the booking
        .select('id', 'quantity')
        .first();

      if (!availableInventory) {
        throw new Error('Inventory not available or fully booked for the specified date.');
      }

      // Decrement the quantity in the inventory by num_guests
      await trx('inventories')
        .where('id', availableInventory.id)
        .decrement('quantity', num_guests); // Decrement quantity by num_guests

      // Insert booking details
      await trx('bookings').insert({
        slot_id: slot_id,
        customer_id: customer_id,
        customer_name: customer_name,
        contact_number: contact_number,
        booking_date: booking_date,
        num_guests: num_guests
      });

      res.json({ message: 'Booking successful' });
    });
  } catch (error) {
    console.error('Error making booking:', error);
    res.status(500).json({ error: error.message });
  }
});

// Function to create inventories for the next 5 days
async function createInventoriesForNextFiveDays() {
  try {
    const currentDate = new Date();
    const inventoriesForDays = [];

    // Fetch slots from the database
    const slots = await db('slots');

    for (let i = 0; i < 5; i++) {
      const date = new Date(currentDate.getTime() + i * 24 * 60 * 60 * 1000);
      const dayInventories = slots.map(slot => ({
        slot_id: slot.id,
        restaurant_id: slot.restaurant_id,
        quantity: slot.capacity,
        date: date.toISOString().split('T')[0]
      }));
      inventoriesForDays.push(...dayInventories);
    }

    // Insert inventories into the database
    await db('inventories').insert(inventoriesForDays);

    return inventoriesForDays;
  } catch (error) {
    throw new Error('Error generating inventories for the next five days: ' + error.message);
  }
}

// Other routes...

const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
