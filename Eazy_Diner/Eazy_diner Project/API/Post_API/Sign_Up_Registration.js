const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const knex = require('knex');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js'); // Import knex configuration

const app = express();
const port = 8888;

app.use(cors());
app.use(bodyParser.json());

const db = knex(knexConfig); // Initialize knex with the configuration from knexfile.js

app.post('/api/signup', async (req, res) => {
  try {
    const { name, email, password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const [userId] = await db('customers').insert({
      name,
      email,
      password: hashedPassword,
    });

    res.json({ message: 'User registered successfully', userId });
  } catch (error) {
    console.error('Error registering user:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.all('/api/signup', (req, res) => {
  res.status(405).json({ error: 'Method Not Allowed' });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
