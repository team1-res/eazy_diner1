const express = require('express');
const cors = require('cors');
const knex = require('knex');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js');

const app = express();
const port = process.env.PORT || 5000;

const db = knex(knexConfig);

app.use(cors());

app.get('/api/restaurants', async (req, res) => {
  const { cuisineType, location, title } = req.query;

  try {
    let query = db('restaurants').select('*');

    // Apply filtering based on query parameters
    if (cuisineType) {
      query = query.whereRaw('LOWER(cuisine_type) LIKE ?', [`%${cuisineType.toLowerCase()}%`]);
    }
    if (location) {
      query = query.whereRaw('LOWER(location) LIKE ?', [`%${location.toLowerCase()}%`]);
    }
    if (title) {
      query = query.whereRaw('LOWER(name) LIKE ?', [`%${title.toLowerCase()}%`]);
    }

    const restaurants = await query;
    res.json(restaurants);
  } catch (error) {
    console.error('Error fetching restaurants:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/restaurants/:restaurantId/slots', async (req, res) => {
  const { restaurantId } = req.params;

  try {
    const slots = await db('slots').where('restaurant_id', restaurantId).select('*');
    res.json(slots);
  } catch (error) {
    console.error('Error fetching slots:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(port, () => {
  console.log(`Listing Server is running on port ${port}`);
});
