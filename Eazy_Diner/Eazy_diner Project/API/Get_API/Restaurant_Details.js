const express = require('express');
const cors = require('cors');
const knex = require('knex');
const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile');

const app = express();
const port = 8088;
const db = knex(knexConfig);

app.use(cors());

app.get('/api/restaurants/:restaurantId', async (req, res) => {
  const restaurantId = req.params.restaurantId;

  try {
    const restaurantDetails = await db('restaurants')
      .select('*')
      .where('id', restaurantId)
      .first();

    if (!restaurantDetails) {
      res.status(404).json({ error: 'Restaurant not found' });
    } else {
      res.json({ restaurant: restaurantDetails });
    }
  } catch (error) {
    console.error('Error fetching restaurant details:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/booking/dates', async (req, res) => {
  try {
    const bookingTimes = await db('slots').select('start_time');

    // Extracting time from start_time
    const times = bookingTimes.map(bookingTime => bookingTime.start_time);

    res.json({ bookingDates: times });
  } catch (error) {
    console.error('Error fetching booking dates:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/booking/slots', async (req, res) => {
  const { restaurant_id } = req.query;

  try {
   

    const bookingSlots = await db('slots')
      .select('id', 'start_time') // Selecting only the 'id' and 'start_time' fields
      .where('restaurant_id', restaurant_id);
      

    res.json({ bookingSlots }); // Sending the bookingSlots directly to the frontend
  } catch (error) {
    console.error('Error fetching booking slots:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
