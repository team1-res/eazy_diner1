const express = require('express');
const bodyParser = require('body-parser');
const knex = require('knex');

const knexConfig = require('/Users/vedanthvasishth/Desktop/restaurant/Eazy_Diner/Eazy_diner Project/knexfile.js');
const db = knex(knexConfig);

const app = express();

app.use(bodyParser.json());


function generateTimeSlots(date) {
  const slots = [];
  const startTime = new Date(date);
  const endTime = new Date(date);
  
  // Set start time to 9 am
  startTime.setHours(9, 0, 0, 0);
  
  // Set end time to 11 pm
  endTime.setHours(23, 0, 0, 0);

  // Add 5 hours and 30 minutes to both start and end times
  startTime.setHours(startTime.getHours() + 5, startTime.getMinutes() + 30);
  endTime.setHours(endTime.getHours() + 5, endTime.getMinutes() + 30);

  while (startTime < endTime) {
    const slot = {
      start_time: startTime.toISOString(),
      // Each slot is now 1 hour
      end_time: new Date(startTime.getTime() + (60 * 60 * 1000)).toISOString(),
      capacity: 20 // Limit to 20 customers per slot
    };
    slots.push(slot);
    startTime.setTime(startTime.getTime() + (60 * 60 * 1000)); // Move to next slot (1 hour)
  }

  return slots;
}

// Function to generate slots for the next week from the current date for each restaurant
async function generateSlotsForNextWeek() {
  try {
    const restaurants = await db('restaurants').select('id');
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() + 1); // Start from tomorrow

    const slotsForWeek = [];

    for (const restaurant of restaurants) {
      const restaurantSlots = [];
      for (let i = 0; i < 1; i++) {
        const slots = generateTimeSlots(currentDate);
        restaurantSlots.push(...slots.map(slot => ({ ...slot, restaurant_id: restaurant.id })));
        currentDate.setDate(currentDate.getDate() + 1); 
      }
      slotsForWeek.push(...restaurantSlots);
    }

    return slotsForWeek;
  } catch (error) {
    throw new Error('Error generating slots for the week: ' + error.message);
  }
}

app.post('/api/slots/create', async (req, res) => {
  try {
    const weekSlots = await generateSlotsForNextWeek();
    await db('slots').insert(weekSlots);
    res.json({ message: 'Slots created successfully' });
  } catch (error) {
    console.error('Error creating slots:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.post('/api/bookings/book', async (req, res) => {
  const { slot_id, customer_id } = req.body;

  try {
    await db.transaction(async (trx) => {
      const slot = await trx('slots')
        .where('id', slot_id)
        .andWhere('capacity', '>', 0)
        .first();

      if (!slot) {
        return res.status(404).json({ error: 'Slot not available or does not exist' });
      }

      await trx('slots')
        .where('id', slot_id)
        .decrement('capacity');

      await trx('bookings').insert({
        slot_id,
        customer_id,
        created_at: db.fn.now(),
        updated_at: db.fn.now()
      });
    });

    res.json({ message: 'Slot booked successfully' });
  } catch (error) {
    console.error('Error booking slot:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

const port = process.env.PORT || 8081;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
